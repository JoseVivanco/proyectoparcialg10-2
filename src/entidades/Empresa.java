/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.*;
import java.util.*;

/**
 *
 * @author Brank
 */
public class Empresa {
    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String nombre;
    private String categoria;
    private String url;
    private String facebook;
    private String twitter;
    private String instagram;
    private ArrayList<Establecimiento> establecimientos;

    public Empresa(String nombre, String categoria, String url, String facebook, String twitter, String instagram) {
        this.codigo = Empresa.codigoIncremental++;
        this.nombre = nombre;
        this.categoria = categoria;
        this.url = url;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
        this.establecimientos = new ArrayList<>();
    }
   
    public static Empresa comprobarEmpresa(String nombreEmpresa) {
        Empresa empresa = null;
        for (Empresa e : Data.empresas) {
            if (e.getNombre().equalsIgnoreCase(nombreEmpresa)) {
                empresa = e;
            }
        }
        return empresa;
    }
    
    public static void registrarEmpresa() {
        System.out.println("Ingrese el nombre de la empresa :");
        String nuevoNombre = Utilidades.ingresoString();
        Empresa empresa = comprobarEmpresa(nuevoNombre);
        if (empresa == null) {
            System.out.println("Ingrese categoría :");
            String nuevaCategoria = Utilidades.ingresoString();
            System.out.println("Ingrese URL :");
            String nuevaUrl = Utilidades.ingresoString();
            System.out.println("Ingrese usuario de Facebook :");
            String nuevoFacebook = Utilidades.ingresoString();
            System.out.println("Ingrese usuario de Twitter :");
            String nuevoTwitter = Utilidades.ingresoString();
            System.out.println("Ingrese usuario de Instagram :");
            String nuevoInstagram = Utilidades.ingresoString();
            Empresa nuevaEmpresa = new Empresa(nuevoNombre, nuevaCategoria, nuevaUrl, nuevoFacebook, nuevoTwitter, nuevoInstagram);
            Data.empresas.add(nuevaEmpresa);
            System.out.println("Empresa agregada con éxito");
        } else {
            System.out.println("La empresa ya existe");
        }
    }
    
    /* GETTERS & SETTERS */
    
    public Integer getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public ArrayList<Establecimiento> getEstablecimientos() {
        return establecimientos;
    }            

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    
}

