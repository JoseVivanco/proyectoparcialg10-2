/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.Utilidades;

/**
 *
 * @author Brank
 */
public class Tarjeta {

    private String nombre;
    private String tipo;

    public Tarjeta(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }

    public static Tarjeta nuevaTarjeta() {
        System.out.println("Ingrese el nombre de la tarjeta :");
        String nombreTarjeta = Utilidades.ingresoString();
        System.out.println("Elija el tipo de tarjeta :");
        System.out.println("1. Crédito\n2. Débito\n3. Afiliación");
        boolean tarjetaAgregada = false;
        String tipoTarjeta = Utilidades.ingresoString();
        while (!(tarjetaAgregada)) {
            switch (tipoTarjeta) {
                case "1":
                    tipoTarjeta = "Crédito";
                    tarjetaAgregada = true;
                    break;
                case "2":
                    tipoTarjeta = "Débito";
                    tarjetaAgregada = true;
                    break;
                case "3":
                    tipoTarjeta = "Afiliación";
                    tarjetaAgregada = true;
                    break;
                default:
                    System.out.print("Opción incorrecta. Elija el tipo de tarjeta : ");
                    tipoTarjeta = Utilidades.ingresoString();
            }
        }
        Tarjeta tarjeta = new Tarjeta(nombreTarjeta, tipoTarjeta);
        return tarjeta;
    }

    /* GETTERS & SETTERS */
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
