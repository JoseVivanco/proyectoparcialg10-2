/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;

/**
 *
 * @author Brank
 */
public class MenuPromociones extends Menu{

    public MenuPromociones(ArrayList<String> opciones) {
        super(opciones);
    }
    
    public static ArrayList<String> añadirOpciones(){
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Todos");
        opciones.add("Por descripción");
        opciones.add("Por días de validez");
        opciones.add("Por tarjeta");
        opciones.add("Regresar al menú principal");
        return opciones;
    }    
}
